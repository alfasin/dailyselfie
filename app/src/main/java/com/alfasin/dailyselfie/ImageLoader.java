package com.alfasin.dailyselfie;

/**
 * Created by alfasi on 8/23/15.
 */
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

public class ImageLoader {

    private static final String TAG = "ImageLoader";

    public void DisplayImage(String imagePath, ImageView imageView) {
        Log.i(TAG, "About to load bitmap: " + imagePath);
        imageView.setImageBitmap(getBitmap(imagePath));
    }

    private Bitmap getBitmap(String fullPath) {
        return BitmapFactory.decodeFile(fullPath);
    }

}
