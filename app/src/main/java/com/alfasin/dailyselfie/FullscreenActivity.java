package com.alfasin.dailyselfie;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;


public class FullscreenActivity extends Activity {

    private static final String TAG = "FullscreenActivity";
    private ImageLoader imageLoader = new ImageLoader();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        ImageView image = (ImageView)findViewById(R.id.full_size);
        Integer position = Integer.valueOf((Integer) getIntent().getSerializableExtra("position"));
        Log.i(TAG, "position: " + position);
        Item item = DailySelfieActivity.images.get(position);
        imageLoader.DisplayImage(item.imageFullPath, image);
    }


}
