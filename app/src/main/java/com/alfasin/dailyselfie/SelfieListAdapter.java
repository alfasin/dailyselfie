package com.alfasin.dailyselfie;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

/**
 * Created by alfasi on 8/23/15.
 */
public class SelfieListAdapter extends ArrayAdapter<Item> {
    private Activity context;
    private List<Item> images;
    private ImageLoader imageLoader;
    private static String TAG = "SelfieListAdapter";

    SelfieListAdapter(Activity context, List<Item> images) {
        super(context, R.layout.item, images);
        this.context = context;
        this.images = images;
        imageLoader = new ImageLoader();
        Log.i(TAG, "finished onCreate");
    }

    public static class ViewHolder {
        public ImageView selfie;
        ViewHolder(View view){
            selfie = (ImageView) view.findViewById(R.id.icon);
        }

        @Override
        public String toString(){
            return "ViewHolder";
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Log.i(TAG, "inside getView; images.get("+position+")=" + images.get(position));
        Log.d(TAG, Arrays.toString(images.toArray()));
        View view = convertView;
        ViewHolder holder;
        if(view == null){
            LayoutInflater vi =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else{
            holder = (ViewHolder)view.getTag();
        }

        if(images.get(position) != null ){
            Item rec = images.get(position);
            Log.d("position: ", position + "; Item:" + rec);

            TextView textView = (TextView) view.findViewById(R.id.label);
            textView.setText(extractFileNameFromPath(rec));

            ImageView imageView = (ImageView) view.findViewById(R.id.icon);
            imageLoader.DisplayImage(rec.imageFullPath, imageView);

            holder.selfie.setTag(position);
        }
        return view;
    }

    private String extractFileNameFromPath(Item rec) {
        return rec.imageFullPath.substring(
                        rec.imageFullPath.lastIndexOf("/") + 1,
                        rec.imageFullPath.length() - 4
                );
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Item getItem(int position) {
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}

class Item implements Comparable {
    public String imageFullPath;

    public Item (String imageFullPath) {
        this.imageFullPath = imageFullPath;
    }

    @Override
    public String toString() {
        return imageFullPath;
    }

    @Override
    public int compareTo(Object another) {
        if (!(another instanceof Item)) {
            throw new IllegalArgumentException(
                    "Cannot compare an instance of Item with: " + another);
        }
        Item o = (Item)another;
        return -imageFullPath.compareTo(o.imageFullPath);
    }
}