package com.alfasin.dailyselfie;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class DailySelfieActivity extends AppCompatActivity {

    private static final String TAG = "DailySelfieActivity";
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static String mCurrentPhotoPath;
    private static File storageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES);
    public final static List<Item> images = new ArrayList<>();
    private SelfieListAdapter selfieAdapter = null;
    private Notification notification = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_selfie);
        loadImageFileList();
        selfieAdapter = new SelfieListAdapter(this, images);
        ListView imageList = ((ListView)findViewById(R.id.images));
        imageList.setAdapter(selfieAdapter);
        imageList.setOnItemClickListener(new OnItemClickListener());
        setTimerForNotification();
    }

    private void setTimerForNotification() {
        MyTimerTask myTask = new MyTimerTask();
        Timer myTimer = new Timer();
        myTimer.schedule(myTask, 2 * 60 * 1000, 2 * 60 * 1000);
    }

    class MyTimerTask extends TimerTask {
        public void run() {
            generateNotification(getApplicationContext(), getString(R.string.take_pic));
        }
    }

    private void generateNotification(Context context, String message) {

        int icon = R.drawable.ic_camera;
        String appname = context.getResources().getString(R.string.app_name);
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, DailySelfieActivity.class), 0);

        notification = getNotification(context, message, icon, appname, currentapiVersion, contentIntent);
        notificationManager.notify(0, notification);
    }

    private Notification getNotification(Context context, String message, int icon, String appname,
                                         int currentapiVersion, PendingIntent contentIntent) {

        if (notification != null)
            return notification;

        // To support 2.3 os
        if (currentapiVersion < android.os.Build.VERSION_CODES.HONEYCOMB) {
            notification = new Notification(icon, message, 0);
            notification.setLatestEventInfo(context, appname, message,
                    contentIntent);
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(
                    context);
            notification = builder.setContentIntent(contentIntent)
                    .setSmallIcon(icon)
                    .setTicker(appname)
                    .setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(appname)
                    .setContentText(message)
                    .build();
        }
        notification.flags = Notification.FLAG_AUTO_CANCEL | Notification.FLAG_ONLY_ALERT_ONCE;
        return notification;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_daily_selfie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_camera) {
            Log.i(TAG, "onOptionsItemSelected(), action_camera.");
            dispatchTakePictureIntent();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if (resultCode == RESULT_OK) {
                Log.i(TAG, "onActivityResult() RESULT_OK = " + data);
                // Image captured and saved to fileUri specified in the Intent
                Toast.makeText(this, "Image saved to:\n" +
                        mCurrentPhotoPath, Toast.LENGTH_SHORT).show();

                Log.i(TAG, "created image from: " + mCurrentPhotoPath.substring(5));
                images.add(0, new Item(mCurrentPhotoPath.substring(5)));
                selfieAdapter.notifyDataSetChanged();

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
                Toast.makeText(this, "Action has been cancelled!", Toast.LENGTH_SHORT).show();
            } else {
                // Image capture failed, advise user
                Toast.makeText(this, "Action failed!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // auxiliary methods
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            Log.i(TAG, "dispatchTakePictureIntent(), in first if");
            // Create the File where the photo should go
            File photoFile = null;
            try {
                Log.i(TAG, "dispatchTakePictureIntent(), going to run: createImageFile()");
                photoFile = createImageFile();
                Log.i(TAG, "dispatchTakePictureIntent(), after createImageFile() " + photoFile);
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(TAG, "failed to create a file!");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                Log.i(TAG, "calling camera!!!");
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "selfie_" + timeStamp;
        Log.i(TAG, "createImageFile() imageFileName: " + imageFileName);
        File image = new File(storageDir, imageFileName + ".jpg");

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void loadImageFileList() {
        Log.i(TAG, "storageDir.getAbsolutePath() = " + storageDir.getAbsolutePath());
        File f = new File(storageDir.getAbsolutePath());
        File[] file = f.listFiles();
        List<Item> result = new ArrayList<>();
        for (int i=0; i < file.length; i++) {
            if (file[i].getName() != null &&
                    file[i].getName().startsWith("selfie_")) {

                Log.d(TAG, "FileName:" + file[i].getName());
                result.add(new Item(storageDir.getAbsolutePath() + File.separator + file[i].getName()));
            }
        }
        Log.i(TAG, Arrays.toString(result.toArray()));
        Collections.sort(result);
        images.addAll(result);
    }
}
