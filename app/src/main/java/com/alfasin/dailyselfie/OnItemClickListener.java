package com.alfasin.dailyselfie;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

/**
 * Created by alfasi on 8/23/15.
 */
public class OnItemClickListener implements AdapterView.OnItemClickListener {

    private static final String TAG = "OnItemClickListener";
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onItemClick() [id:" + id + ", position = " + position + "]");
        Context context = parent.getContext();
        Log.d(TAG, "Going to update an existing Feed");
        Intent intent = new Intent(context, FullscreenActivity.class);
        intent.putExtra("position", position);
        context.startActivity(intent);
    }
}
